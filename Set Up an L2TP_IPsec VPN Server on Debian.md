# Set Up an L2TP_IPsec VPN Server on Debian
![image](https://github.com/nu11secur1ty/Set-Up-an-L2TP_IPsec-VPN-Server-on-Debian/blob/master/linux-vpn-tux.png)

# Step 1: Initial setup

You’ll need to have set up a Cloud Server running Linux. The steps in this tutorial assume that you are using Debian Linux, but should be similar for other versions of Linux or BSDs if you have a preference. We recommend running all the commands below as root, or using sudo.

If you are looking to use the VPN to connect to several servers within ElasticHosts, make sure that the others are connected to the VPN server by a VLAN as described in our tutorial on VLANs.

If you don’t intend to connect to other machines within your ElasticHosts account (for example, if you want to use the VPN for increased privacy while browsing), you won’t need the second server. Finally, if you’re using a firewall such as iptables or the built-in ElasticHosts firewall, you’ll need to make sure that UDP traffic is allowed to port 500 (IKE) and port 4500 (for IPsec Nat traversal). For the purposes of this tutorial, we will give our VPN server an address of 10.0.5.1 on the VLAN, and connect a second server over the VLAN at 10.0.5.2. If you are not using a VLAN, you can add an internal address to your server’s eth0 device as follows:

```
$ vim /etc/network/interfaces
```

```
#These lines, or something similar to them, should already exist
auto lo  
iface lo inet loopback

auto eth0  
iface eth0 inet dhcp

#We need to add the lines below this one
auto eth0:1  
iface eth0:1 inet static  
  address 10.0.5.1
  netmask 255.255.255.0
```

Next, we need to enable IP forwarding. Using your text editor of choice, open your server’s sysctl configuration file:

```
$ vim /etc/sysctl.conf
```

This allows you to change several operating system parameters within Linux. We’re going to add three lines: the first enables IP forwarding, and is essential. 
The other two lines disable ICMP redirects: this is not essential but is highly recommended unless you believe they are specifically required.

```
net.ipv4.ip_forward = 1  
net.ipv4.conf.all.accept_redirects = 0  
net.ipv4.conf.all.send_redirects = 0  
```

Tell sysctl to re-read the configuration file to start using the new parameters.

```
$ sysctl -p /etc/sysctl.conf
```

Finally, we must install the packages we will be using – the Openswan IPsec VPN, and xl2tpd, the Layer 2 Tunneling Protocol Daemon.


```
$ apt-get update
$ apt-get install openswan xl2tpd
```

When installing Openswan, you will be asked whether you want to create an X.509 certificate. This tutorial will cover preshared key (PSK) authentication, so you can select No here: if you change your mind at any time you can reach this prompt again by running the command dpkg-reconfigure openswan.

- Important: While PSK authentication is secure enough for most uses, this may leave servers vulnerable to ‘Man in the Middle’ (MitM) attacks, potentially allowing a malicious server to masquerade as the VPN gateway. While this is only possible if the attacker is in possession of the PSK, authentication with X.509 certificates or with RSA keypairs makes this type of attack significantly more difficult. If you are planning to allow VPN clients to use the server’s internet connection, you may also wish to install the iptables-persistent package, as this will come in useful later.

```
$ apt-get install iptables-persistent
```

# Step 2: IPsec configuration
First, we’ll configure the IPsec part of our network: this will provide a secure channel for our L2TP-tunnelled data. 
We’ll start by editing the main Openswan configuration file as follows.

```
$ vim /etc/ipsec.conf
```

```
config setup  
nat_traversal=yes  
virtual_private=%v4:10.0.0.0/8,%v4:192.168.0.0/16,%v4:172.16.0.0/12  
oe=off  
protostack=netkey  
conn L2TP-PSK  
authby=secret  
pfs=no  
auto=add  
keyingtries=3  
rekey=yes  
ikelifetime=8h  
keylife=1h  
type=transport  
left=

[lns default]
ip range = 10.0.5.50-10.0.5.255  
local ip = 10.0.5.1  
refuse chap = yes  
refuse pap = yes  
require authentication = yes  
name=elastichosts-vpn-server  
ppp debug = yes  
pppoptfile = /etc/ppp/options.xl2tpd  
length bit = yes  
```

- local ip – This is the IP of your VLAN interface. As mentioned above, we are using 10.0.5.1 in this example.
- ip range – A range of IPs from which internal addresses will be assigned to remote clients. This should be on the same subnet as this machine, but should not conflict with addresses in use on the VLAN. Here, we specify 10.0.5.50-255, allowing for plenty of servers within the VLAN and over 200 potential remote users.
- We have specified refuse chap and refuse pap. When we specify PPP options we will choose to require the use of MS-CHAPv2, for better compatibility with Windows clients.

Now the PPP options.

```
$ vim /etc/ppp/options.xl2tpd
```

```
require-mschap-v2  
ms-dns 8.8.8.8  
ms-dns 4.2.2.1  
ms-dns 8.8.4.4  
proxyarp  
asyncmap 0  
auth  
crtscts  
lock  
hide-password  
modem  
debug  
```


- ms-dns – Some clients will request DNS settings from the server when connected over the VPN. Here, we are simply using public DNS provided by Google and Level3.
- proxyarp – Proxy ARP is required to rewrite the source hardware address and prevent traffic being dropped for coming from an invalid location.

Finally, the CHAP secret for PPP authentication.

```
$ vim /etc/ppp/chap-secrets
```

```
# Secrets for authentication using CHAP
# client        server                           secret                       IP addresses
*               elastichosts-vpn-server          a-secure-chap-secret         *
```


Again, you can specify several lines following this format, in order to provide different secrets for different users.

- client – This allows you to set a username to go with the CHAP secret. Here, we use the wildcard * to allow any username.
- server – The name of the server. We specified this in /etc/xl2tpd/xl2tpd.conf above.
- secret – The CHAP secret itself. Again, this is a password and should follow good practice for password strength.
- IP addresses – An IP address or range of addresses that will be used to allocate an address to any user who connects using this CHAP secret. 
    This should be a subset of the range specified above: here we have used * once again, meaning the server should allocate addresses from the whole range.

# Step 4 (Optional): Enable NAT for remote clients
In order for remote users to connect to the public Internet through this VPN server, the gateway must be configured to perform Network Address Translation (NAT) for remote clients. 
To do that, we will use Linux’s built-in iptables firewall: the following command tells it to perform NAT for the IP range 10.0.5.0/24.
This nearly completes our tutorial – if you would like to make this server’s internet connection available for remote clients to use a proxy, you must follow one more step. 
If you are simply aiming to use your IPsec VPN to connect remote users to servers within an ElasticHosts VLAN, you can skip to the final step: starting Openswan.


```
$ iptables -t nat -A POSTROUTING -o eth0 -s 10.0.5.0/24 -j MASQUERADE
```
You may recall that we installed the package iptables-persistent during the first step. 
This provides a simple method of ensuring that the firewall rules we create will be loaded when the server boots. 
To save the firewall rules, run:

```
$ iptables-save > /etc/iptables/rules.v4
```

Remote users can now set a default route through this server in order to securely access the internet through the VPN.
# Step 5: Start Openswan

All we need to do now is start Openswan and xl2tpd. On Debian, we can do this as follows:

```
$ /etc/init.d/ipsec start
$ /etc/init.d/xl2tpd start
```

To make Openswan and xl2tpd start automatically on boot, simply run the following two commands.

```
$ update-rc.d ipsec defaults
$ update-rc.d xl2tpd defaults
```

# You're finished!
# Source: [link:](https://www.elastichosts.com/)
